##Notes

A note about installing CosmoSIS in Chinese

https://bitbucket.org/AnyUsernameNotExist/generalnotes/wiki/InstallCosmoSIS

Use CosmoSIS to produce not only GG spectrum, but also GI and II. Change redshift distribution n(z)

https://bitbucket.org/AnyUsernameNotExist/generalnotes/wiki/IA%20spectrums

Install CAMB

https://bitbucket.org/AnyUsernameNotExist/generalnotes/wiki/InstallCAMB

Install CosmoSIS on OSX

https://bitbucket.org/AnyUsernameNotExist/generalnotes/wiki/OSX%20CosmoSIS

Install CosmoSIS on Ubuntu

https://bitbucket.org/AnyUsernameNotExist/generalnotes/wiki/Install%20Cosmosis%20on%20Ubuntu

##Account Introduction

This account includes codes that produce:
this paper: https://arxiv.org/abs/1707.01072, old Fortran code and new CosmoSIS-python code

2nd paper on Self-Calibration (under review)

reproduced correlation function of this paper: https://arxiv.org/abs/astro-ph/0509026v2, matlab, TreeCorr-python
codes for project CFHTLenS + VIPERS (failed), matlab, TreeCorr-python

Self-project using Maple

Self-Calibration detection using KiDS/CFHTLenS survey data

Paper LaTex drafts